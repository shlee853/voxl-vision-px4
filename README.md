# voxl-vision-px4

This service acts as the main hub managing communication between VOXL and PX4.

dependencies:
* libvoxl_io
* librc_math
* libmodal_pipe
* libmodal_json
* voxl-mpa-tools

This README covers building this package. For more details on how to use it and how to set up the PX4, see the ModalAI documentation webpage here: https://docs.modalai.com/docs/flight-user-guides/voxl-vision-px4/

## Build Instructions

1) prerequisite: voxl-emulator docker image >= v1.1

Follow the instructions here:

https://gitlab.com/voxl-public/voxl-docker


2) load the git submodule(s).

```bash
git submodule update --init --recursive
```

3) Launch Docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/voxl-vision-px4$ voxl-docker -i voxl-emulator
bash-4.3$ ls
README.md   build.sh  install_deps_in_docker.sh  ipk       src
build-deps  clean.sh  install_on_voxl.sh         make_package.sh
```

4) Install dependencies inside the docker. Specify the dependencies should be pulled from either the development (dev) or stable modalai package repos. If building the master branch you should specify `stable`, otherwise `dev`.

```bash
./install_build_deps.sh stable
```


5) Compile inside docker.

```bash
bash-4.3$ ./build.sh
```

5) Make an ipk package inside docker.

```bash
bash-4.3$ ./make_package.sh

Package Name:  voxl_vision_px4
version Number:  x.x.x
...

Done making voxl_vision_px4_x.x.x.ipk
```

This will make a new voxl_vision_px4_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.


## Deploy to VOXL

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/voxl-vision-px4$ ./install_on_voxl.sh
pushing voxl_vision_px4_0.0.1_8x96.ipk to target
searching for ADB device
adb device found
voxl_vio_to_px4_0.0.1_8x96.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package voxl_vision_px4 from root...
Installing voxl_vision_px4 (0.0.1) on root.
Configuring voxl_vision_px4.

Done installing voxl_vision_px4
```

