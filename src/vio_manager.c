/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <rc_math.h>
#include <modal_pipe_client.h>
#include <modal_pipe_server.h>

#include "macros.h"
#include "vio_manager.h"
#include "pipe_channels.h"
#include "geometry.h"
#include "uart_mavlink.h"
#include "udp_mavlink.h"
#include "px4_monitor.h"
#include "config_file.h"


// We could grab these from the qvio server and modal vins packages but I don't
// want to add two big build dependencies just for their static names
#define QVIO_PIPE	(MODAL_PIPE_DEFAULT_BASE_DIR "qvio/")
#define VINS_PIPE	(MODAL_PIPE_DEFAULT_BASE_DIR "vins/")

// debug modes set by setter functions
static int print_debug_local;
static int print_debug_fixed;

// vio server state exists to allow the VIO server to start and stop in the
// background without voxl-vision-px4 needing to restart. This also enables the
// user to swap between QVIO and VINS
#define SS_WAITING			0 // waiting for a connection
#define SS_DISCONNECTED		1 // just disconnected and needs closing
#define SS_CONNECTED		2 // connected and working
static int server_state;
static pthread_t thread_id;
static int running;


void vio_manager_en_print_debug_local(int en_print)
{
	print_debug_local = en_print;
	return;
}

void vio_manager_en_print_debug_fixed(int en_print)
{
	print_debug_fixed = en_print;
	return;
}


// called when new valid data is received through the VIO pipe helper
static void process_new_vio_data(vio_data_t d)
{
	int i;
	mavlink_message_t msg;
	static uint8_t last_vio_state = VIO_STATE_FAILED;

	// warn if VIO failed and print the error
	if(d.state == VIO_STATE_FAILED && last_vio_state != VIO_STATE_FAILED){
		fprintf(stderr, "VIO reported failure\n");
		modal_vio_print_error_code(d.state);
		last_vio_state = d.state;
		return;
	}
	last_vio_state = d.state;

	// just return if vio is still initializing or still in failed state
	if(d.state != VIO_STATE_OK) return;


	// gravity update function also serves to see if vio was initialized
	// upside down or really off-level.
	int ret = geometry_update_gravity_offset(d.gravity_vector);
	// if not successful, reset and try again
	if(ret==-2){
		geometry_bump_reset_counter();
		pipe_client_send_control_cmd(VIO_PIPE_CH, RESET_VIO_HARD);
		return;
	}
	// some other failure, don't continue
	if(ret<0) return;


	// copy out position & rotation, camera and imu relation, send into geometry module
	geometry_update_imu_to_vio(d.timestamp_ns, d.T_imu_wrt_vio, d.R_imu_to_vio);
	geometry_update_cam_to_imu(d.T_cam_wrt_imu, d.R_cam_to_imu);

	// Fetch converted rotation and position for px4
	static rc_matrix_t R_body_to_local = RC_MATRIX_INITIALIZER;
	static rc_vector_t T_body_wrt_local = RC_VECTOR_INITIALIZER;
	geometry_get_R_body_to_local(&R_body_to_local);
	geometry_get_T_body_wrt_local(&T_body_wrt_local);

	// convert to quaternion for px4. px4 wants q from local to body
	// so invert the quaternion
	float q_for_px4[4];
	static rc_vector_t q_body_to_local = RC_VECTOR_INITIALIZER;
	rc_rotation_to_quaternion(R_body_to_local, &q_body_to_local);
	for(i=0;i<4;i++) q_for_px4[i] = q_body_to_local.d[i];
	for(i=1;i<4;i++) q_for_px4[i] *= -1.0f; // invert q

	// angular rates copy and transform
	// this isn't used by ekf2 but send anyway for completeness
	static rc_vector_t w_imu_wrt_imu = RC_VECTOR_INITIALIZER;
	static rc_vector_t w_body_wrt_body = RC_VECTOR_INITIALIZER;
	float_to_vector(d.imu_angular_vel, &w_imu_wrt_imu);
	geometry_rotate_vec_from_imu_to_body_frame(w_imu_wrt_imu, &w_body_wrt_body);

	// velocity copy and transform
	// mavlink odometry message expects velocity in body frame even though
	// position is in local frame so after converting from vioframe to local ned
	// frame we need to further rotate into body frame. This will be undone in
	// px4 and rotated back to local ned frame before going into ekf2
	static rc_vector_t v_imu_wrt_vio = RC_VECTOR_INITIALIZER;
	static rc_vector_t v_body_wrt_body = RC_VECTOR_INITIALIZER;
	float_to_vector(d.vel_imu_wrt_vio, &v_imu_wrt_vio);
	geometry_calc_velocity_in_body_frame(v_imu_wrt_vio, w_imu_wrt_imu, &v_body_wrt_body);

	// qvio reports very small covarience values and vins fusion doesn't
	// report covarience at all. When testing sending qvio's tiny covarience
	// to PX4, EKF2 just increases it to the PX4 EKF2_EV_NOISE parameter as a
	// lower bound. Reducing this lower bound results in EKF2 misbehaving.
	// so ignore the covarience by just sending NAN and tune with EKF2_EV_NOISE
	float mav_pose_covariance[21];
	float mav_vel_covariance[21];
	for(i=0;i<21;i++) mav_pose_covariance[i] = NAN;
	for(i=0;i<21;i++) mav_vel_covariance[i] = NAN;

	// VIO rotation is sent in as body to local frame
	// VIO position is sent as body position with respect to local frame
	// VIO velocity is sent in aligned with body frame
	// vio angular rates are sent in aligmed with body frame
	uint8_t frame_id = MAV_FRAME_LOCAL_FRD;
	uint8_t child_frame_id = MAV_FRAME_BODY_FRD;

	// Send timestamp as-is. This timestamp comes from clock-monotonic
	// PX4 will do its own timesyncing since we respond to its timesync requests.
	uint64_t usec = d.timestamp_ns/1000;

	// send data if PX4 is online and we have registered its sysid
	mavlink_msg_odometry_pack(  px4_monitor_get_sysid(), \
								VOXL_COMPID, \
								&msg, \
								usec, \
								frame_id, \
								child_frame_id, \
								T_body_wrt_local.d[0], \
								T_body_wrt_local.d[1], \
								T_body_wrt_local.d[2], \
								q_for_px4, \
								v_body_wrt_body.d[0], \
								v_body_wrt_body.d[1], \
								v_body_wrt_body.d[2], \
								w_body_wrt_body.d[0], \
								w_body_wrt_body.d[1], \
								w_body_wrt_body.d[2], \
								mav_pose_covariance, \
								mav_vel_covariance, \
								geometry_get_reset_counter(), \
								MAV_ESTIMATOR_TYPE_VIO);

	// only send to PX4 if it's connected
	if(px4_monitor_is_connected()){
		uart_mavlink_send_msg(msg);
	}

	// if anyone is subscribed to the local pose pipe, write latest data to it
	if(pipe_server_get_num_clients(LOCAL_POSE_OUT_CH)>0){
		pose_vel_6dof_t p;
		p.magic_number = POSE_VEL_6DOF_MAGIC_NUMBER;
		p.timestamp_ns = d.timestamp_ns;
		vector_to_float(T_body_wrt_local, p.T_child_wrt_parent);
		matrix_to_float(R_body_to_local, p.R_child_to_parent);
		// todo input velocity into geometry ring buffer earlier and retrieve here
		RC_VECTOR_ON_STACK(v_body_wrt_local,3);
		geometry_calc_velocity_in_local_frame(v_imu_wrt_vio, w_imu_wrt_imu, &v_body_wrt_local);
		vector_to_float(v_body_wrt_local, p.v_child_wrt_parent);
		vector_to_float(w_body_wrt_body, p.w_child_wrt_child);
		pipe_server_send_to_channel(LOCAL_POSE_OUT_CH, (char*)&p, sizeof(p));
	}

	// if anyone is subscribed to the fixed pose pipe, write latest data to it
	if(pipe_server_get_num_clients(FIXED_POSE_OUT_CH)>0){
		pose_vel_6dof_t p;
		p.magic_number = POSE_VEL_6DOF_MAGIC_NUMBER;
		p.timestamp_ns = d.timestamp_ns;
		static rc_vector_t T_body_wrt_fixed = RC_VECTOR_INITIALIZER;
		static rc_matrix_t R_body_to_fixed = RC_MATRIX_INITIALIZER;
		geometry_get_T_body_wrt_fixed(&T_body_wrt_fixed);
		geometry_get_R_body_to_fixed(&R_body_to_fixed);
		vector_to_float(T_body_wrt_fixed, p.T_child_wrt_parent);
		matrix_to_float(R_body_to_fixed, p.R_child_to_parent);
		// todo input velocity into geometry ring buffer earlier and retrieve here
		RC_VECTOR_ON_STACK(v_body_wrt_local,3);
		RC_VECTOR_ON_STACK(v_body_wrt_fixed,3);
		geometry_calc_velocity_in_local_frame(v_imu_wrt_vio, w_imu_wrt_imu, &v_body_wrt_local);
		geometry_rotate_vec_from_local_to_fixed_frame(v_body_wrt_local, &v_body_wrt_fixed);
		vector_to_float(v_body_wrt_fixed, p.v_child_wrt_parent);
		vector_to_float(w_body_wrt_body, p.w_child_wrt_child);
		pipe_server_send_to_channel(FIXED_POSE_OUT_CH, (char*)&p, sizeof(p));
	}

	// optionally send to ground station over udp
	// if using offboard mode on PX4 you will get odometry packets from PX4 and
	// VOXL but they will have different component IDs. VOXL is component ID 197
	if(en_send_vio_to_qgc){
		udp_mavlink_send_msg(msg);
	}

	// print local odometry and body velocity as it was sent to px4
	if(print_debug_local){
		double roll, pitch, yaw;
		rc_rotation_to_tait_bryan(R_body_to_local, &roll, &pitch, &yaw);
		printf("T_body_wrt_local: %5.2f %5.2f %5.2f  RPY: %5.2f %5.2f %5.2f  velocity: %5.2f %5.2f %5.2f\n", \
			T_body_wrt_local.d[0], T_body_wrt_local.d[1], T_body_wrt_local.d[2],\
			roll, pitch, yaw,\
			v_body_wrt_body.d[0],  v_body_wrt_body.d[1],  v_body_wrt_body.d[2]);
	}

	// print odometry with respect to fixed frame. This is NOT sent to px4,
	// this is just a debug tool.
	if(print_debug_fixed){
		double roll, pitch, yaw;
		static rc_matrix_t R_body_to_fixed = RC_MATRIX_INITIALIZER;
		static rc_vector_t T_body_wrt_fixed = RC_VECTOR_INITIALIZER;
		geometry_get_R_body_to_fixed(&R_body_to_fixed);
		geometry_get_T_body_wrt_fixed(&T_body_wrt_fixed);
		rc_rotation_to_tait_bryan(R_body_to_fixed, &roll, &pitch, &yaw);
		printf("T_body_wrt_fixed: %5.2f %5.2f %5.2f  RPY: %5.2f %5.2f %5.2f\n", \
			T_body_wrt_fixed.d[0], T_body_wrt_fixed.d[1], T_body_wrt_fixed.d[2],\
			roll, pitch, yaw);
	}

	return;
}


// This is the callback assigned to the client pipe interface. It checks for
// server disconnects and validates the received vio data before processing it.
// Multiple packets may come through at once although that's not typical.
static void vio_pipe_helper_cb(int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// reading from a pipe will return 0 if the other end (server) closes
	// check for this (and negative  numbers indicating error) and quit.
	// your program doesn't need to quit, you can handle the server quitting
	// however you like to suite your needs. We just quit for this example.
	if(bytes<=0){
		fprintf(stderr, "VIO Server Disconnected from channel %d\n", ch);
		server_state = SS_DISCONNECTED;
		return;
	}

	// validate that the data makes sense
	int n_packets, i;
	vio_data_t* data_array = modal_vio_validate_pipe_data(data, bytes, &n_packets);
	if(data_array == NULL) return;
	for(i=0;i<n_packets;i++) process_new_vio_data(data_array[i]);
	return;
}


static void _update_imu(void)
{
	char info_string[512];
	if(pipe_client_get_info_string(VIO_PIPE_CH, info_string, 512)<1){
		fprintf(stderr, "WARNING: failed to get info string from vio server\n");
		return;
	}

	// todo handle other IMUs
	if(strstr(info_string, "imu0") != NULL){
		geometry_set_imu("imu0");
	}
	else if(strstr(info_string, "imu1") != NULL){
		geometry_set_imu("imu1");
	}
	else{
		fprintf(stderr, "WARNING: VIO info string contained neither imu0 or imu1\n");
	}
	return;
}


// keep an eye on the vio server pipe. If disconnected, keep trying both qvio
// and vins fusion sources. Once one of them is connected, keep an eye on if its
// still connected and try to reset the connection if possible. This allows the user
// to stop and start one or the other VIO server in the background and swap between them
// without restarting voxl-vision-px4
static void* vio_pipe_monitor(__attribute__((unused)) void* arg)
{
	while(running){
		if(server_state==SS_WAITING){
			int ret;
			// attempt to connect to the server expecting it might fail
			ret = pipe_client_init_channel(VIO_PIPE_CH, QVIO_PIPE, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_SIMPLE_HELPER, VIO_RECOMMENDED_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to qvio server
				server_state = SS_CONNECTED;
				printf("Connected to voxl-qvio-server\n");
				_update_imu();
				pipe_client_set_simple_helper_cb(VIO_PIPE_CH, vio_pipe_helper_cb, NULL);
				continue;
			}
			// next try vins if qvio failed
			ret = pipe_client_init_channel(VIO_PIPE_CH, VINS_PIPE, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_SIMPLE_HELPER, VIO_RECOMMENDED_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to VINS server
				server_state = SS_CONNECTED;
				printf("Connected to voxl-vins-server\n");
				pipe_client_set_simple_helper_cb(VIO_PIPE_CH, vio_pipe_helper_cb, NULL);
				continue;
			}
		}
		else if(server_state==SS_DISCONNECTED){
			// cleanup if disconnected and go back to waiting
			pipe_client_close_channel(VIO_PIPE_CH);
			server_state = SS_WAITING;
		}
		// otherwise all is fine.
		// TODO check time since last vio packet here as an added safety measure?
		usleep(500000);
	}

	// close channel when we exit
	pipe_client_close_channel(VIO_PIPE_CH);
	return NULL;
}


int vio_manager_start(void)
{
	if(running){
		fprintf(stderr, "ERROR in %s, already running\n", __FUNCTION__);
		return 0;
	}
	// start the server pipes to send local and fixed data out
	int flags = 0; // no flags needed right now
	if(pipe_server_init_channel(LOCAL_POSE_OUT_CH, BODY_WRT_LOCAL_POSE_PATH, flags)) return -1;
	if(pipe_server_init_channel(FIXED_POSE_OUT_CH, BODY_WRT_FIXED_POSE_PATH, flags)) return -1;
	// start the manager thread, this will open the pipe
	running = 1;
	pthread_create(&thread_id, NULL, vio_pipe_monitor, NULL);
	return 0;
}


// stop VIO callback immediately, then wait for manager thread to stop
void vio_manager_stop(void)
{
	if(running==0) return;

	// stop the manager thread, this will close the pipe
	running = 0;
	pthread_join(thread_id, NULL);
	// close server pipes
	pipe_server_close_channel(LOCAL_POSE_OUT_CH);
	pipe_server_close_channel(FIXED_POSE_OUT_CH);
	return;
}

