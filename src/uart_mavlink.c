/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

//#define DEBUG

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include "config_file.h"
#include "uart_mavlink.h"
#include "px4_monitor.h"
#include "udp_mavlink.h"
#include "geometry.h"
#include "px4_shell.h"


#define BUF_LEN		(32*1024) // big 32k read buffer, each message is 291 bytes
#define BUF_PACKETS	(BUF_LEN/(int)sizeof(mavlink_message_t))

static int running = 0;
static mavlink_message_t* msg_buf; // big rpc shared memory buffer
static uint8_t* status_buf;				// to hold status of each read message

// local variables
static pthread_t thread_id;
static pthread_mutex_t uart_write_mutex = PTHREAD_MUTEX_INITIALIZER;
static int print_debug_send = 0;
static int print_debug_recv = 0;

// local functions
static void _message_handler(mavlink_message_t msg, uint8_t status);
static void* _thread_func(void* arg);



void uart_mavlink_en_print_debug_send(int en_print_debug)
{
	print_debug_send = en_print_debug;
	return;
}

void uart_mavlink_en_print_debug_recv(int en_print_debug)
{
	print_debug_recv = en_print_debug;
	return;
}



// this function is called once a complete message is parsed from the uart port
// connected to PX4
static void _message_handler(mavlink_message_t msg, uint8_t status)
{
	if(print_debug_recv){
		printf("uart recv msgid: %3d sysid:%3d compid:%3d status: %d\n", msg.msgid, msg.sysid, msg.compid, status);
	}

	// no CRC/signature errors, do local handling
	if(status == MAVLINK_FRAMING_OK){

		// this is a fast function, just copies any useful data out
		px4_monitor_scrape_data(&msg);

		// special case for handling the timesync packet since we use that locally
		if(msg.msgid==MAVLINK_MSG_ID_TIMESYNC){
			int64_t now = voxl_apps_time_monotonic_ns();
			// Message originating from PX4 system, timestamp and return it
			if(mavlink_msg_timesync_get_tc1(&msg) == 0){
				mavlink_message_t return_msg;
				mavlink_msg_timesync_pack(msg.sysid, VOXL_COMPID, &return_msg,\
											now, mavlink_msg_timesync_get_ts1(&msg));
				uart_mavlink_send_msg(return_msg);
				if(print_debug_recv){
					printf("uart_mavlink responding to timesync request\n");
				}

				return;
			}
			// if message has tc1!=0 then something other than PX4 is requesting this
			// timesync, probably mavros. block it for now
			// TODO handle this more gracefully?
			return;
		}

		// handle serial control packets that also go to pc4_shell module
		if(msg.msgid==MAVLINK_MSG_ID_SERIAL_CONTROL){
			px4_shell_handle_serial_msg_from_px4(msg);
		}
	}

	// send ALL message out UDP to QGC/mavros, even those with CRC/signature
	// errors since the error is likely not an integrity issue, just a mismatch
	// in version, or the user has their own custom mavlink dialect that the
	// receiving end can decode without error.
	udp_mavlink_send_msg(msg);
	return;
}



// note, reading must be done in a pthread, not in main()!!!
// the signal handler will interfere with the RPC read call otherwise
static void* _thread_func(__attribute__((unused))void* context)
{
	// this is a tight loop around the read function. Read will wait to return
	// untill there is either at least one message available or there was an error
	// this no need to sleep
	while(running){
		// this blocks for up to 1/2 second waiting for a mavlink message
		int msg_read = voxl_mavparser_read(px4_uart_bus, (char*)msg_buf, status_buf);

		// handle unsuccessful returns
		if(msg_read>=BUF_PACKETS) fprintf(stderr, "WARNING, mavlink buffer full!\n");
		if(msg_read<0){
			fprintf(stderr, "ERROR voxl_mavparser_read returned %d, exiting uart thread\n", msg_read);
			fprintf(stderr, "perhaps something else is using mavparser in the background?\n");
			px4_monitor_set_connected_flag(0);
			running = 0;
			return NULL;
		}

		// if we went 1/2 second with no messages we disconnected
		if(msg_read==0){
			px4_monitor_set_connected_flag(0);
			fprintf(stderr, "WARNING PX4 DISCONNECTED FROM UART\n");
			continue;
		}

		// pass each packet with status into message handler
		for(int i=0;i<msg_read;i++) _message_handler(msg_buf[i], status_buf[i]);
	}

	return NULL;
}




int uart_mavlink_init(void)
{
	// initialize the read buffer and mavparser
	msg_buf = (mavlink_message_t*)voxl_rpc_shared_mem_alloc(BUF_LEN);
	status_buf = voxl_rpc_shared_mem_alloc(BUF_PACKETS);
	if(msg_buf==NULL || status_buf==NULL){
		fprintf(stderr, "failed to allocate shared memory buffers for mavparser\n");
		return -1;
	}

	// try up to 10 times to start mavparser, this is beacause the sdsp
	// may still be trying to initialize shortly after boot
	// TODO verify this is necessary and that we don't just crash sompletely
	// if this fails the first time
	int i, did_init=0;
	for(i=0;i<10;i++){
		if(voxl_mavparser_init(px4_uart_bus, px4_uart_baudrate, BUF_LEN, 0)){
			fprintf(stderr, "failed to initialize mavparser, trying again\n");
			usleep(500000);
		}
		else{
			printf("Successfully opened mavparser\n");
			did_init = 1;
			break;
		}
	}

	if(!did_init){
		fprintf(stderr, "ERROR failed to initialize mavparser after 10 attempts\n");
		return -1;
	}

	// start the read thread
	running = 1;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	if(pthread_create(&thread_id, &attr, _thread_func, NULL) != 0){
		fprintf(stderr, "couldn't start thread\n");
		return -1;
	}

	// test sending a msg
	mavlink_message_t msg;
	mavlink_msg_heartbeat_pack(0, VOXL_COMPID, &msg, MAV_TYPE_ONBOARD_CONTROLLER, \
								MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
	if(uart_mavlink_send_msg(msg)){
		fprintf(stderr, "ERROR in uart_mavlink_init, failed to send test packet\n");
		return -1;
	}

	return 0;
}


int uart_mavlink_stop(void)
{
	if(running==0){
		fprintf(stderr, "ERROR in uart_mavlink_stop, listener not running\n");
		return -1;
	}

	// Wait for thread to return
	running = 0;
	pthread_join(thread_id, NULL);
	voxl_mavparser_close(px4_uart_bus);
	voxl_rpc_shared_mem_free((uint8_t*)msg_buf);
	pthread_mutex_destroy(&uart_write_mutex);
	printf("uart_mavlink completely stopped\n");
	return 0;
}


int uart_mavlink_send_msg(mavlink_message_t msg)
{
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int msg_len;

	if(!running) return -1;

	if(print_debug_send){
		printf("uart send msgid: %3d sysid:%3d compid:%3d\n", msg.msgid, msg.sysid, msg.compid);
	}

	memset(buf, 0, MAVLINK_MAX_PACKET_LEN);
	msg_len = mavlink_msg_to_send_buffer(buf, &msg);
	if(msg_len < 0){
		fprintf(stderr, "ERROR: in uart_mavlink_send_msg, unable to pack message for sending\n");
		return -1;
	}
	if(uart_mavlink_send_bytes(buf, msg_len)!=msg_len){
		return -1;
	}
	return 0;
}

int uart_mavlink_send_bytes(uint8_t* data, int bytes)
{
	int ret;

	if(!running) return -1;

	if(bytes <=0 ){
		fprintf(stderr, "WARNING in uart_mavlink_send_bytes, asked to send %d bytes\n", bytes);
		return -1;
	}
	// TODO: need to test writing packets >128bytes
	// there was an old QC bug long ago with uart port not liking long writes,
	// not sure if it was fixed or not
	pthread_mutex_lock(&uart_write_mutex);
	ret = voxl_uart_write(px4_uart_bus, data, bytes);
	pthread_mutex_unlock(&uart_write_mutex);

	if(ret != bytes){
		fprintf(stderr, "WARNING: in uart_mavlink_send_bytes: voxl_uart_write returned %d, expected %d\n",
				ret, bytes);
	}
	return ret;
}


int uart_mavlink_send_heartbeat(void)
{
	mavlink_message_t msg;
	if(!px4_monitor_is_connected()) return -1;
	mavlink_msg_heartbeat_pack(px4_monitor_get_sysid(), VOXL_COMPID, &msg, MAV_TYPE_ONBOARD_CONTROLLER, \
								MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
	return uart_mavlink_send_msg(msg);
}


int uart_mavlink_send_local_setpoint(uint8_t sysid, uint8_t compid, mavlink_set_position_target_local_ned_t local_sp)
{
	mavlink_message_t msg;
	if(!px4_monitor_is_connected()) return -1;
	local_sp.target_system = px4_monitor_get_sysid();
	mavlink_msg_set_position_target_local_ned_encode(sysid,compid, &msg, &local_sp);
	uart_mavlink_send_msg(msg);
	return 0;
}

int uart_mavlink_send_fixed_setpoint(uint8_t sysid, uint8_t compid, mavlink_set_position_target_local_ned_t fixed_sp)
{
	mavlink_message_t msg;
	mavlink_set_position_target_local_ned_t local_sp;
	if(!px4_monitor_is_connected()) return -1;
	geometry_transform_fixed_setpoint_to_local(fixed_sp, &local_sp);
	local_sp.target_system = px4_monitor_get_sysid();
	mavlink_msg_set_position_target_local_ned_encode(sysid,compid, &msg, &local_sp);
	uart_mavlink_send_msg(msg);
	return 0;
}