/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <modal_json.h>
#include "config_file.h"
#include "geometry.h"

////////////////////////////////////////////////////////////////////////////////
// stuff from common config
////////////////////////////////////////////////////////////////////////////////

// from apriltags.conf
int n_fixed_apriltags;
fixed_apriltag_t fixed_apriltags[VCC_MAX_APRILTAGS_IN_CONFIG];
// from extrinsics.conf
rc_matrix_t R_imu_to_body = RC_MATRIX_INITIALIZER;
rc_vector_t T_imu_wrt_body = RC_VECTOR_INITIALIZER;
double height_body_above_ground_m;
rc_vector_t T_stereo_wrt_body = RC_VECTOR_INITIALIZER;
rc_matrix_t R_stereo_to_body = RC_MATRIX_INITIALIZER;
rc_vector_t T_vio_ga_wrt_local = RC_VECTOR_INITIALIZER;
rc_matrix_t R_vio_ga_wrt_local = RC_MATRIX_INITIALIZER;

int load_common_config_files(void)
{
	int i,j,k,n;
	vcc_apriltag_t tags[VCC_MAX_APRILTAGS_IN_CONFIG];
	vcc_extrinsic_t t[VCC_MAX_EXTRINSICS_IN_CONFIG];
	vcc_extrinsic_t tmp;

	// read in all apriltags, including static and dynamic ones
	if(vcc_read_apriltag_conf_file(VCC_APRILTAG_CONFIG_PATH, tags, &n, VCC_MAX_APRILTAGS_IN_CONFIG)){
		return -1;
	}
	vcc_print_apriltag_conf(tags, n);

	// pick out the fixed ones and convert to rc_vector rc_matrix types
	int nf = 0; // number of fixed tags found
	for(i=0;i<n;i++){
		// skip non-fixed tags
		if(tags[n].location!=LOCATION_FIXED) continue;
		// copy the tag_id and allocate vectors/matrices
		fixed_apriltags[nf].tag_id = tags[n].tag_id;
		rc_matrix_alloc(&fixed_apriltags[nf].R_tag_to_fixed, 3,3);
		rc_vector_alloc(&fixed_apriltags[nf].T_tag_wrt_fixed, 3);
		rc_matrix_alloc(&fixed_apriltags[nf].R_fixed_to_tag, 3,3);
		rc_vector_alloc(&fixed_apriltags[nf].T_fixed_wrt_tag, 3);
		// copy in data
		for(j=0; j<3; j++){
			fixed_apriltags[nf].T_tag_wrt_fixed.d[j] = tags[i].T_tag_wrt_fixed[j];
			for(k=0; k<3; k++){
				fixed_apriltags[nf].R_tag_to_fixed.d[j][k] = tags[i].R_tag_to_fixed[j][k];
			}
		}
		// also calculate inverse for later use
		geometry_invert_tf( fixed_apriltags[nf].R_tag_to_fixed,\
							fixed_apriltags[nf].T_tag_wrt_fixed,\
							&fixed_apriltags[nf].R_fixed_to_tag,\
							&fixed_apriltags[nf].T_fixed_wrt_tag);
		// keep track of how many fixed tags
		nf++;
	}
	// save the result of our counter
	n_fixed_apriltags = nf;
	printf("loaded in %d fixed apriltags\n", nf);


	// now load in extrinsics
	if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, t, &n, VCC_MAX_EXTRINSICS_IN_CONFIG)){
		return -1;
	}
	vcc_print_extrinsic_conf(t, n);

	// Pick out IMU to Body TODO pick which imu to use
	if(vcc_find_extrinsic_in_array("body", "imu1", t, n, &tmp)){
		fprintf(stderr, "ERROR: %s missing imu1 to body transform\n", VCC_EXTRINSICS_PATH);
		return -1;
	}
	rc_vector_from_array(&T_imu_wrt_body, tmp.T_child_wrt_parent, 3);
	rc_matrix_alloc(&R_imu_to_body,3,3);
	for(j=0; j<3; j++){
		for(k=0; k<3; k++) R_imu_to_body.d[j][k] = tmp.R_child_to_parent[j][k];
	}

	// pick out body to stereo
	if(vcc_find_extrinsic_in_array("body", "stereo_l", t, n, &tmp)){
		fprintf(stderr, "ERROR: %s missing stereo to body transform\n", VCC_EXTRINSICS_PATH);
		return -1;
	}
	rc_vector_from_array(&T_stereo_wrt_body, tmp.T_child_wrt_parent, 3);
	rc_matrix_alloc(&R_stereo_to_body,3,3);
	for(j=0; j<3; j++){
		for(k=0; k<3; k++) R_stereo_to_body.d[j][k] = tmp.R_child_to_parent[j][k];
	}

	// pick out height above ground
	if(vcc_find_extrinsic_in_array("body", "ground", t, n, &tmp)){
		fprintf(stderr, "ERROR: %s missing ground to body transform\n", VCC_EXTRINSICS_PATH);
		return -1;
	}
	rc_vector_alloc(&T_vio_ga_wrt_local,3);
	T_vio_ga_wrt_local.d[0] = T_imu_wrt_body.d[0];
	T_vio_ga_wrt_local.d[1] = T_imu_wrt_body.d[1];
	T_vio_ga_wrt_local.d[2] = T_imu_wrt_body.d[2] - tmp.T_child_wrt_parent[2];

	return 0;
}





////////////////////////////////////////////////////////////////////////////////
// stuff from our own config file
////////////////////////////////////////////////////////////////////////////////

#define FILE_HEADER "\
/**\n\
 * VOXL Vision PX4 Configuration File\n\
 *\n\
 */\n"


// define all the "Extern" variables from config_file.h, defaults where possible
// UDP mavlink router
char qgc_ip[64];
int en_localhost_mavlink_udp;
int en_secondary_qgc;
char secondary_qgc_ip[64];
int qgc_udp_port_number;
int localhost_udp_port_number;
// basic visual features
int en_vio;
int en_voa;
int en_send_vio_to_qgc;
int en_send_voa_to_qgc;
// adsb
int en_adsb;
int adsb_uart_bus;
int adsb_uart_baudrate;
// px4 uart
int px4_uart_bus;
int px4_uart_baudrate;
// offboard mode config
offboard_mode_t offboard_mode;
int follow_tag_id;
// fixed frame
int en_apriltag_fixed_frame;
int fixed_frame_filter_len;
int en_transform_mavlink_pos_setpoints_from_fixed_frame;



int config_file_print(void)
{
	const char* offboard_strings[] = OFFBOARD_STRINGS;
	printf("=================================================================");
	printf("\n");
	printf("Parameters as loaded from config file:\n");
	printf("qgc_ip:                     %s\n", qgc_ip);
	printf("en_localhost_mavlink_udp    %d\n", en_localhost_mavlink_udp);
	printf("en_secondary_qgc:           %d\n", en_secondary_qgc);
	printf("secondary_qgc_ip:           %s\n", secondary_qgc_ip);
	printf("qgc_udp_port_number:        %d\n", qgc_udp_port_number);
	printf("localhost_udp_port_number:  %d\n", localhost_udp_port_number);
	printf("en_vio:                     %d\n", en_vio);
	printf("en_voa:                     %d\n", en_voa);
	printf("en_send_vio_to_qgc:         %d\n", en_send_vio_to_qgc);
	printf("en_send_voa_to_qgc:         %d\n", en_send_voa_to_qgc);
	printf("en_adsb:                    %d\n", en_adsb);
	printf("adsb_uart_bus:              %d\n", adsb_uart_bus);
	printf("adsb_uart_baudrate:         %d\n", adsb_uart_baudrate);
	printf("px4_uart_bus:               %d\n", px4_uart_bus);
	printf("px4_uart_baudrate:          %d\n", px4_uart_baudrate);
	printf("offboard_mode:              %s\n", offboard_strings[offboard_mode]);
	printf("follow_tag_id:              %d\n", follow_tag_id);
	printf("en_apriltag_fixed_frame:    %d\n", en_apriltag_fixed_frame);
	printf("fixed_frame_filter_len:     %d\n", fixed_frame_filter_len);
	printf("en_transform_mavlink_pos_setpoints_from_fixed_frame:%d\n",\
			en_transform_mavlink_pos_setpoints_from_fixed_frame);
	printf("=================================================================");
	printf("\n");
	return 0;
}


int config_file_load(void)
{
	const char* offboard_strings[] = OFFBOARD_STRINGS;
	const int n_modes = sizeof(offboard_strings)/sizeof(offboard_strings[0]);

	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(VOXL_VISION_PX4_CONF_FILE,\
																		FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new empty json file: %s\n", VOXL_VISION_PX4_CONF_FILE);

	// read the data in
	cJSON* parent = json_read_file(VOXL_VISION_PX4_CONF_FILE);
	if(parent==NULL) return -1;

	// parse each item
	// UDP mavlink router
	json_fetch_string_with_default(	parent, "qgc_ip", qgc_ip, 63, "192.168.8.60");
	json_fetch_bool_with_default(	parent, "en_localhost_mavlink_udp", &en_localhost_mavlink_udp, 1);
	json_fetch_bool_with_default(	parent, "en_secondary_qgc", &en_secondary_qgc, 0);
	json_fetch_string_with_default(	parent, "secondary_qgc_ip", secondary_qgc_ip, 63, "192.168.1.214");
	json_fetch_int_with_default(	parent, "qgc_udp_port_number", &qgc_udp_port_number, 14550);
	json_fetch_int_with_default(	parent, "localhost_udp_port_number", &localhost_udp_port_number, 14551);
	// basic visual features
	json_fetch_bool_with_default(	parent, "en_vio", &en_vio, 1);
	json_fetch_bool_with_default(	parent, "en_voa", &en_voa, 1);
	json_fetch_bool_with_default(	parent, "en_send_vio_to_qgc", &en_send_vio_to_qgc, 0);
	json_fetch_bool_with_default(	parent, "en_send_voa_to_qgc", &en_send_voa_to_qgc, 0);
	// adsb
	json_fetch_bool_with_default(	parent, "en_adsb", &en_adsb, 1);
	json_fetch_int_with_default(	parent, "adsb_uart_bus", &adsb_uart_bus, 7);
	json_fetch_int_with_default(	parent, "adsb_uart_baudrate", &adsb_uart_baudrate, 57600);
	// px4 uart
	json_fetch_int_with_default(	parent, "px4_uart_bus", &px4_uart_bus, 5);
	json_fetch_int_with_default(	parent, "px4_uart_baudrate", &px4_uart_baudrate, 921600);
	// offboard mode
	json_fetch_enum_with_default(	parent, "offboard_mode", (int*)&offboard_mode, offboard_strings, n_modes, 1);
	json_fetch_int_with_default(	parent, "follow_tag_id", &follow_tag_id, 0);
	// fixed frame
	json_fetch_bool_with_default(	parent, "en_apriltag_fixed_frame", &en_apriltag_fixed_frame, 0);
	json_fetch_int_with_default(	parent, "fixed_frame_filter_len", &fixed_frame_filter_len, 5);
	json_fetch_bool_with_default(	parent, "en_transform_mavlink_pos_setpoints_from_fixed_frame", &en_transform_mavlink_pos_setpoints_from_fixed_frame, 0);

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", VOXL_VISION_PX4_CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(VOXL_VISION_PX4_CONF_FILE, parent, FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}

