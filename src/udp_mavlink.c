/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>	// for  close
#include <errno.h>
#include <voxl_io.h>

#include "udp_mavlink.h"
#include "uart_mavlink.h"
#include "config_file.h"
#include "geometry.h"
#include "offboard_mode.h"

#define UDP_READ_BUF_LEN	1024
#define MAV_CHAN			0
#define MAX_CONNECTIONS		16
#define LOCAL_ADDRESS_INT	2130706433


// todo, maybe add a status flag to keep track of lost connections
typedef struct mav_connection_t{
	int open;
	struct sockaddr_in sockaddr;
	char ip[16];
}mav_connection_t;


static mav_connection_t connections[MAX_CONNECTIONS];
static int running;
static int sockfd_qgc;
static int sockfd_local;
static struct sockaddr_in si_me_qgc; // my sockaddr for talking to all qgc connections
static struct sockaddr_in si_me_local;  // my sockaddr for talking to localhost
static struct timeval tv; // for recv timeout
static pthread_t recv_thread_id_qgc;
static pthread_t recv_thread_id_local;
static int print_debug_send;
static int print_debug_recv;


// add a new IP to the list of connectinos to send mavlink to
static void __add_connection(unsigned long ip)
{
	if(ip==LOCAL_ADDRESS_INT){
		fprintf(stderr,"ERROR, can't add localhost as a QGC connection\n");
		return;
	}

	// array of connections is filled from the beginning
	// fill the first entry that's not been opened yet unless we find
	// another entry with the same IP
	for(int i=0;i<MAX_CONNECTIONS;i++){
		if(connections[i].sockaddr.sin_addr.s_addr == ip) return;
		else if(connections[i].open == 0){
			connections[i].sockaddr.sin_family = AF_INET; // IPv4
			connections[i].sockaddr.sin_port = htons(qgc_udp_port_number);
			connections[i].sockaddr.sin_addr.s_addr = ip;
			sprintf(connections[i].ip,"%s",inet_ntoa(connections[i].sockaddr.sin_addr));
			connections[i].open = 1;
			printf("Added new UDP connection to %s\n", connections[i].ip);
			return;
		}
	}
	return;
}

// this function is called once a complete message is parsed from either udp port
static void _message_handler(mavlink_message_t* msg)
{
	// send signal to stop our own offboard mode if something else is trying to
	// take over
	if( msg->msgid == MAVLINK_MSG_ID_SET_POSITION_TARGET_LOCAL_NED ||\
		msg->msgid == MAVLINK_MSG_ID_SET_POSITION_TARGET_GLOBAL_INT){
		offboard_mode_stop(0); // 0 == nonblocking mode!
	}

	// if enabled, translate position setpoints to local frame
	if(msg->msgid == MAVLINK_MSG_ID_SET_POSITION_TARGET_LOCAL_NED && \
		en_transform_mavlink_pos_setpoints_from_fixed_frame){
		//printf("converting setpoint to local frame, sys %d comp: %d\n", msg->sysid, msg->compid);
		mavlink_set_position_target_local_ned_t sp;
		uint8_t sysid=msg->sysid;
		uint8_t compid=msg->compid;
		mavlink_msg_set_position_target_local_ned_decode(msg, &sp);
		uart_mavlink_send_fixed_setpoint(sysid,compid,sp);
		return;
	}

	/*
	// for debugging serial commands from qgc
	if(msg->msgid==126){
		printf("UDP recv msg: ID: %d sys:%d comp:%d\n", msg->msgid, msg->sysid, msg->compid);
		printf("device: %d\n",mavlink_msg_serial_control_get_device(msg));
		printf("flags: %d\n",mavlink_msg_serial_control_get_flags(msg));
		printf("timeout: %d\n",mavlink_msg_serial_control_get_timeout(msg));
		printf("baudrate: %d\n",mavlink_msg_serial_control_get_baudrate(msg));
		printf("count: %d\n",mavlink_msg_serial_control_get_count(msg));
		uint8_t buf[128];
		mavlink_msg_serial_control_get_data(msg,buf);
		printf("data: %s\n",buf);
		printf("done\n");
	}
	*/

	// every other message, just send to px4
	uart_mavlink_send_msg(*msg);
	return;
}


void udp_mavlink_en_print_debug_send(int en_print_debug)
{
	printf("Enabling UDP send debugging\n");
	print_debug_send = en_print_debug;
	return;
}

void udp_mavlink_en_print_debug_recv(int en_print_debug)
{
	printf("Enabling UDP recv debugging\n");
	print_debug_recv = en_print_debug;
	return;
}


int udp_mavlink_send_msg(mavlink_message_t msg)
{
	int bytes_sent, i;
	if(!running) return -1;

	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int msg_len = mavlink_msg_to_send_buffer(buf, &msg);

	// send to any open connections
	for(i=0;i<MAX_CONNECTIONS;i++){
		if(connections[i].open){
			bytes_sent = sendto(sockfd_qgc, buf, msg_len, MSG_CONFIRM, \
								(const struct sockaddr*) &connections[i].sockaddr, \
								sizeof(connections[i].sockaddr));

			if(print_debug_send){
				printf("UDP sent bytes: %3d msg id: %3d to %s\n", bytes_sent, msg.msgid, connections[i].ip);
			}
		}
	}

	// optionally send to local port
	if(en_localhost_mavlink_udp){
		bytes_sent = sendto(sockfd_local, buf, msg_len, MSG_CONFIRM, \
								(const struct sockaddr*) &si_me_local, sizeof(si_me_local));

		if(print_debug_send){
			printf("UDP sent bytes: %3d msg id: %3d to localhost\n", bytes_sent, msg.msgid);
		}
	}

	return 0;
}

// thread to read from qgc_udp_port_number
static void* __recv_thread_func(__attribute__((unused)) void *vargp)
{
	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	int msg_received = 0;
	char buf[UDP_READ_BUF_LEN];
	struct sockaddr_in si_other;
	socklen_t slen = sizeof(si_other);

	// keep going until udp_mavlink_stop sets running to 0
	while(running){

		// Receive UDP message from ground control, this is blocking with timeout
		bytes_read = recvfrom(sockfd_qgc, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
								(struct sockaddr*)&si_other, &slen);
		// ignore EAGAIN error, that just means the timeout worked
		if(bytes_read<0 && errno!=EAGAIN){
			perror("ERROR: UDP recvfrom had a problem");
		}

		// ignore local packets
		if(si_other.sin_addr.s_addr == LOCAL_ADDRESS_INT){
			fprintf(stderr, "ERROR, received UDP packet from localhost on port %d\n", qgc_udp_port_number);
			fprintf(stderr, "use port %d for local traffic instead\n", localhost_udp_port_number);
		}

		// do the mavlink byte-by-byte parsing
		for(i=0; i<bytes_read; i++){
			msg_received = mavlink_parse_char(MAV_CHAN, buf[i], &msg, &status);

			// check for dropped packets
			if(status.packet_rx_drop_count != 0){
				fprintf(stderr,"WARNING: UDP listener dropped %d packets\n", status.packet_rx_drop_count);
			}

			// msg_received indicates this byte was the end of a complete packet
			if(msg_received){
				if(print_debug_recv){
					printf("UDP recv msg ID: %3d sysid:%3d from port:%6d IP: %s \n",\
							msg.msgid, msg.sysid, ntohs(si_other.sin_port), \
							inet_ntoa(si_other.sin_addr));
				}

				// for heartbeats, qgc might be trying to establish a new connection
				if(msg.msgid == MAVLINK_MSG_ID_HEARTBEAT){
					__add_connection(si_other.sin_addr.s_addr);
				}
				// send to uart
				_message_handler(&msg);
			}
		}
	}

	printf("exiting QGC udp listener thread\n");
	return NULL;
}

// thread to read local port localhost_udp_port_number
static void* __recv_local_thread_func(__attribute__((unused)) void *vargp)
{
	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	int msg_received = 0;
	char buf[UDP_READ_BUF_LEN];
	struct sockaddr_in si_other; // don't look at this since we expect to just read from localhost
	socklen_t slen = sizeof(si_other);

	// keep going until udp_mavlink_stop sets running to 0
	while(running){

		// Receive UDP message from ground control, this is blocking with timeout
		bytes_read = recvfrom(sockfd_local, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
								(struct sockaddr*)&si_other, &slen);
		// ignore EAGAIN error, that just means the timeout worked
		if(bytes_read<0 && errno!=EAGAIN){
			perror("ERROR: UDP recvfrom local had a problem");
		}

		// do the mavlink byte-by-byte parsing
		for(i=0; i<bytes_read; i++){
			msg_received = mavlink_parse_char(MAV_CHAN, buf[i], &msg, &status);

			// check for dropped packets
			if(status.packet_rx_drop_count != 0){
				fprintf(stderr,"WARNING: UDP local listener dropped %d packets\n", status.packet_rx_drop_count);
			}

			// msg_received indicates this byte was the end of a complete packet
			if(msg_received){
				if(print_debug_recv){
					printf("UDP recv msg ID: %3d sysid:%3d from port:%6d IP: %s \n",\
							msg.msgid, msg.sysid, ntohs(si_other.sin_port), \
							inet_ntoa(si_other.sin_addr));
				}
				_message_handler(&msg);
			}
		}
	}

	printf("exiting localhost udp listener thread\n");
	return NULL;
}


int udp_mavlink_init(void)
{
	// set up new socket
	sockfd_qgc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd_qgc < 0) {
		perror("ERROR: UDP recv socket creation failed");
		return -1;
	}

	// set timeout for the socket
	tv.tv_sec = 0;
	tv.tv_usec = 500000;
	setsockopt(sockfd_qgc, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

	// set up my own sockaddr
	si_me_qgc.sin_family = AF_INET; // IPv4
	si_me_qgc.sin_port = htons(qgc_udp_port_number);
	si_me_qgc.sin_addr.s_addr = htonl(INADDR_ANY);

	// bind socket to port
	if(bind(sockfd_qgc , (struct sockaddr*)&si_me_qgc, sizeof(si_me_qgc)) == -1){
		perror("ERROR: UDP socket failed to bind to port");
		return -1;
	}

	// add any of the 3 ips available for hard-coding in config file
	struct in_addr new_ip;
	printf("Adding manual QGC IP address to udp connection list: %s\n", qgc_ip);
	if(inet_aton(qgc_ip, &new_ip)!=0) __add_connection(new_ip.s_addr);
	else fprintf(stderr, "WARNING: invalid qgc_ip, this is fine if you don't want to use it\n");

	if(en_secondary_qgc){
		printf("Adding manual secondary QGC IP address to udp connection list: %s\n", qgc_ip);
		if(inet_aton(secondary_qgc_ip, &new_ip)!=0) __add_connection(new_ip.s_addr);
		else fprintf(stderr, "WARNING: invalid secondary_qgc_ip, this is fine if you don't want to use it\n");
	}

	running = 1;
	pthread_create(&recv_thread_id_qgc, NULL, __recv_thread_func, NULL);


	// optionally start the localhost socket for mavros
	if(en_localhost_mavlink_udp){
		// set up new socket
		sockfd_local = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sockfd_local < 0) {
			perror("ERROR: local UDP socket creation failed");
			return -1;
		}

		// set timeout for the socket
		setsockopt(sockfd_local, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

		// set up my own sockaddr
		si_me_local.sin_family = AF_INET; // IPv4
		si_me_local.sin_port = htons(localhost_udp_port_number);
		si_me_local.sin_addr.s_addr = inet_addr("127.0.0.1");

		pthread_create(&recv_thread_id_local, NULL, __recv_local_thread_func, NULL);
	}

	return 0;
}


int udp_mavlink_stop(void)
{
	running=0;
	pthread_join(recv_thread_id_qgc, NULL);
	close(sockfd_qgc);
	if(en_localhost_mavlink_udp){
		pthread_join(recv_thread_id_local, NULL);
		close(sockfd_local);
	}
	printf("udp_mavlink stopped\n");
	return 0;
}