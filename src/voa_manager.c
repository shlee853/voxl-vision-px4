/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <modal_pipe_client.h>

#include "voa_manager.h"
#include "pipe_channels.h"
#include "uart_mavlink.h"
#include "udp_mavlink.h"
#include "px4_monitor.h"
#include "config_file.h"
#include "macros.h"
#include "geometry.h"

// each point is 12 bytes, 2k buf gives us up to 169 tag detections each
// read from the pipe which is plenty considering we only expect 5-20 in normal
// operation. This is not the size of the pipe FIFO which can hold much more!
#define PIPE_READ_BUF_SIZE	2048
#define OBSTACLE_PIPE		(MODAL_PIPE_DEFAULT_BASE_DIR "stereo_obstacles/")

#define UPPER_BOUND_M		-0.75
#define LOWER_BOUND_M		0.75
#define MAX_DISTANCE_CM		2000	// [cm] TODO: need to fiddle with min/max
#define MIN_DISTANCE_CM		20		// [cm]
#define N_BINS				13		// MUST BE ODD. each bin is 5 degrees wide
									// 13 bins covers 65 degrees which is about the
									// FOV of the stereo cameras
									//
// server state exists to allow an external data server process to start and stop in the
// background without voxl-vision-px4 needing to restart.
#define SS_WAITING			0 // waiting for a connection
#define SS_DISCONNECTED		1 // just disconnected and needs closing
#define SS_CONNECTED		2 // connected and working
static int server_state;

static int running = 0; // local running flag separate from main_running
static pthread_t thread_id;
static int print_linescan = 0;


void voa_manager_en_print_linescan(int en_print)
{
	print_linescan = en_print;
	return;
}


/**
 * okay so here's how the data binning works. mavlink message has an array of
 * distances of max length 72 so if this was a laser scanner you could divide
 * 360 degrees into 5 degree "bins" with the 0th bin facing forward and working
 * to the right from there. We have a narrower FOV and only use some of the bins.
 *
 * Bin 0 is "straight ahead" and covers the range -2.5 to +2.5 degrees
 */
static int __angle_to_bin(double a)
{
	// convert rad to degrees
	a = RAD_TO_DEG(a);
	int bin = (int)roundf(a/5.0);
	if(bin<0) bin+=72; // wrap around
	if(bin>71){
		fprintf(stderr, "WARNING in VOA: bin out of bounds: %d\n", bin);
		bin=71;
	}
	return bin;
}


// This is the callback assigned to the client pipe interface.
// Multiple packets may come through at once although that's not typical.
//
// TODO: currently the camera and point cloud helpers don't provide a way to
// detect a disconnect, only the simple helper which VIO and Apriltag managers
// use. Add disconnect detection to libmodal_pipe and this in the future
static void obstacle_pipe_helper_cb(__attribute__((unused))int ch, \
									point_cloud_metadata_t meta, float* points, \
									__attribute__((unused)) void* context)
{
	mavlink_message_t msg;
	int i, ret;
	int n_points = meta.n_points;
	uint16_t distances[72];

	static rc_vector_t T_point_wrt_stereo = RC_VECTOR_INITIALIZER;
	rc_vector_alloc(&T_point_wrt_stereo,3);
	static rc_vector_t T_point_wrt_level = RC_VECTOR_INITIALIZER;
	rc_vector_alloc(&T_point_wrt_level,3);

	// zero out the distance array marking bins as not used
	for(i=0;i<72;i++) distances[i]=UINT16_MAX;

	// now set the bins in our FOV as max_distance +1 means no obstacle is present.
	distances[0]=MAX_DISTANCE_CM+1;
	for(i=1;i<=(N_BINS/2);i++){
		distances[i]=MAX_DISTANCE_CM+1;
		distances[72-i]=MAX_DISTANCE_CM+1;
	}

	// fetch the appropriate transform from old stereo frame at timestamp
	// to current leveled-out body frame
	static rc_matrix_t R_stereo_to_level = RC_MATRIX_INITIALIZER;
	static rc_vector_t T_stereo_wrt_level = RC_VECTOR_INITIALIZER;
	ret = geometry_get_RT_stereo_to_level(meta.timestamp_ns, &R_stereo_to_level, &T_stereo_wrt_level);

	// TODO PX4 doesn't like not having obstacle data
	// on failure keep sending a message that just says free to move
	if(ret) n_points = 0;

	// for each point detected from stereo, populate the appropriate bin
	for(i=0; i<n_points; i++){

		// put xyz data into our own vector
		T_point_wrt_stereo.d[0] = points[(3*i)+0];
		T_point_wrt_stereo.d[1] = points[(3*i)+1];
		T_point_wrt_stereo.d[2] = points[(3*i)+2];

		// transform to body frame.
		rc_matrix_times_col_vec(R_stereo_to_level, T_point_wrt_stereo, &T_point_wrt_level);
		rc_vector_sum_inplace(&T_point_wrt_level, T_stereo_wrt_level);

		// reject any points above/below the vertical bounds
		if(T_point_wrt_level.d[2]<UPPER_BOUND_M) continue;
		if(T_point_wrt_level.d[2]>LOWER_BOUND_M) continue;

		// calculate distance in cm to send to px4
		double x = T_point_wrt_level.d[0];
		double y = T_point_wrt_level.d[1];
		uint16_t dist_cm = (uint16_t)(sqrtf(x*x + y*y)*100);

		// bound points in min/max
		if(dist_cm<MIN_DISTANCE_CM) dist_cm = MIN_DISTANCE_CM;
		if(dist_cm>MAX_DISTANCE_CM) dist_cm = MAX_DISTANCE_CM;

		// x is forward, y is to the right, positive angle to the right
		int bin = __angle_to_bin(atan2(y,x));

		// record if this point is closer than previously detected obstacle in this bin
		if(dist_cm < distances[bin]) distances[bin] = dist_cm;
	}

	// optional debug print
	if(print_linescan){
		static int first_run = 1;

		if(first_run){
			printf("\n  angle (deg):");
			for(i=-(N_BINS/2); i<=(N_BINS/2); i++) printf("%4d|",i*5);
			printf("\n");
		first_run = 0;
		}
		printf(    "\robstacles [m]:");
		for(i=(72-(N_BINS/2));i<72;i++){
			if(distances[i]>MAX_DISTANCE_CM) printf(" 0.0|");
			else printf("%4.1f|",distances[i]/100.0);
		}
		for(i=0;i<=(N_BINS/2);i++){
			if(distances[i]>MAX_DISTANCE_CM) printf(" 0.0|");
			else printf("%4.1f|",distances[i]/100.0);
		}
		fflush(stdout);
	}

	// don't need to send data if px4 isn't connected. do this check after
	// print_linescan so we can debug this without px4 needing to be attached
	if(!px4_monitor_is_connected()) return;

	// pack and send
	mavlink_msg_obstacle_distance_pack(px4_monitor_get_sysid(), VOXL_COMPID,   \
		 &msg, 0, MAV_DISTANCE_SENSOR_UNKNOWN, distances, 5, MIN_DISTANCE_CM,  \
		 MAX_DISTANCE_CM, 0.0, 0, MAV_FRAME_BODY_FRD);

	if(uart_mavlink_send_msg(msg)){
		fprintf(stderr, "failed to send VOA data out uart to PX4\n");
		return;
	}

	// optionally send to ground station too
	if(en_send_voa_to_qgc){
		udp_mavlink_send_msg(msg);
	}

	return;
}



// this thread just manages the connection to the obstacle server, waiting for
// it to become available, connecting if so, and reconnecting if the server
// restarts. It also closes the pipe connection on exit. No actual processing.
//
// TODO: currently the camera and point cloud helpers don't provide a way to
// detect a disconnect, only the simple helper which VIO and Apriltag managers
// use. Add disconnect detection to libmodal_pipe and this in the future
static void* obstacle_pipe_monitor(__attribute__((unused)) void* arg)
{
	while(running){
		if(server_state==SS_WAITING){
			// attempt to connect to the server expecting it might fail
			int ret = pipe_client_init_channel(OBSTACLE_PIPE_CH, OBSTACLE_PIPE, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_POINT_CLOUD_HELPER, PIPE_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to the server
				server_state = SS_CONNECTED;
				pipe_client_set_point_cloud_helper_cb(OBSTACLE_PIPE_CH, obstacle_pipe_helper_cb, NULL);
				continue;
			}

		}
		else if(server_state==SS_DISCONNECTED){
			// cleanup if disconnected and go back to waiting
			pipe_client_close_channel(OBSTACLE_PIPE_CH);
			server_state = SS_WAITING;
		}
		// otherwise all is fine.
		usleep(500000);
	}

	// close channel when we exit
	pipe_client_close_channel(OBSTACLE_PIPE_CH);
	return NULL;
}


// start the pipe monitor thread which will open the obstacle pipe and helper
int voa_manager_start(void)
{
	if(running){
		fprintf(stderr, "ERROR in %s, already running\n", __FUNCTION__);
		return -1;
	}
	// start the manager thread, this will open the pipe to read obstacle data
	running = 1;
	pthread_create(&thread_id, NULL, obstacle_pipe_monitor, NULL);
	return 0;
}


void voa_manager_stop(void)
{
	if(running==0) return;
	// stop the thread, this will close the pipe
	running = 0;
	pthread_join(thread_id, NULL);
	return;
}
