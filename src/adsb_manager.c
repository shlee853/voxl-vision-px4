/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <voxl_io.h>

#include "adsb_manager.h"
#include "config_file.h"
#include "uart_mavlink.h"
#include "udp_mavlink.h"

#define READ_BUF_LEN 1024

static int running = 0;
static pthread_t thread_id;
static int en_print_debug = 0;


static void *adsb_thread(__attribute__((unused))  void *vargp)
{
	mavlink_message_t msg;
	mavlink_status_t  msg_status;
	int last_message_sequence = 0;
	int bytes_read = 0;

	uint8_t* read_buf = voxl_rpc_shared_mem_alloc(READ_BUF_LEN);
	if(read_buf == NULL) {
		fprintf(stderr, "ERROR starting adb thread, failed to allocate shared rpc memory\n");
		return NULL;
	}

	printf("ADSB thread starting\n");

	while(running){
		usleep(1000);

		bytes_read = voxl_uart_read(adsb_uart_bus, read_buf, READ_BUF_LEN);

		// normal mavlink parsing method
		for(int i=0; i<bytes_read; i++){
			if(mavlink_parse_char(0, read_buf[i], &msg, &msg_status)){
				// reject unexpected messages
				if(msg.msgid != MAVLINK_MSG_ID_ADSB_VEHICLE){
					fprintf(stderr, "WARNING, received non-ADSB message on ADSB uart port, msgid=%d\n", msg.msgid);
					continue;
				}
				// print info and sequence data if enabled
				if(en_print_debug){
					printf("Recv ADSB msg: ICAO: %d, lat: %d long: %d\n",    \
							mavlink_msg_adsb_vehicle_get_ICAO_address(&msg), \
							mavlink_msg_adsb_vehicle_get_lat(&msg),          \
							mavlink_msg_adsb_vehicle_get_lon(&msg));
					if(msg.seq != last_message_sequence + 1){
						fprintf(stderr, "WARNING ADSB MSG SEQUENCE FAIL, expected seq: %d, recevied: %d\n",\
							last_message_sequence + 1, msg.seq);
					}
					// loop sequence around
					if (msg.seq == 255) last_message_sequence = -1;
					else last_message_sequence = msg.seq;
				}

				// reject packets with no position data
				if(mavlink_msg_adsb_vehicle_get_lat(&msg)==0 || \
				   mavlink_msg_adsb_vehicle_get_lon(&msg)==0){
					fprintf(stderr, "WARNING, received ADSB message with zero lat and long\n");
					continue;
				}

				// finally, if all checks passed, send to QGC
				if(udp_mavlink_send_msg(msg)){
					fprintf(stderr, "ERROR in adbs-uart, failed to send msg out udp\n");
				}
			}
		}
	}

	voxl_rpc_shared_mem_free(read_buf);
	printf("ADSB thread ending\n");
	return NULL;
}


void adsb_manager_en_print_debug(void)
{
	en_print_debug=1;
}


int adsb_manager_start(void)
{
	if(voxl_uart_init(adsb_uart_bus, adsb_uart_baudrate)){
		fprintf(stderr, "ERROR initializing ADS-B UART\n");
		return -1;
	}

	// Create thread to ADSB reports and send them off to QGC
	running = 1;
	pthread_create(&thread_id, NULL, adsb_thread, NULL);

	return 0;
}


int adsb_manager_stop(void)
{
	if(running==0){
		fprintf(stderr, "ERROR in adsb_manager_stop, not running\n");
		return -1;
	}
	running = 0;
	pthread_join(thread_id, NULL);
	if(voxl_uart_close(adsb_uart_bus)){
		fprintf(stderr, "ERROR closing ADS-B uart bus %d\n", adsb_uart_bus);
	}

	return 0;
}