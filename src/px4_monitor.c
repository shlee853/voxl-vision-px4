/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <pthread.h>
#include "px4_monitor.h"

// connected flag set to 1 by the scrape_data function and set to 0 by the uart
// mavlink module when mavparser times out
static int is_connected=0;

// mavlink messages sent by VOXL inherit the system id of PX4, therefore
// multiple drones with different system ids can coexist
static uint8_t sysid = 0;

// Telemetry data that needs to be pulled from the relevant mavlink messages
static uint8_t  heartbeat_base_mode         = 0;
static uint32_t heartbeat_custom_mode       = 0;
static uint8_t  heartbeat_system_status     = 0;

// voltages already converted to volts/percent when read from message
static double   sys_status_battery_volts    = 0.0;
static double   sys_status_battery_percent  = 0.0;

// protect multi-byte states such as the attitude struct with this mutex
static pthread_mutex_t data_mutex = PTHREAD_MUTEX_INITIALIZER;

// mavlink attitude struct straight out of packet
static mavlink_attitude_t attitude;


void px4_monitor_scrape_data(mavlink_message_t* msg)
{
    // always monitor the sysid of px4 in case it changes which may happen
    // during setup and config
    if(sysid != msg->sysid){
        sysid = msg->sysid;
        printf("Detected PX4 Mavlink SYSID %d\n", sysid);
    }

    switch(msg->msgid){
    case MAVLINK_MSG_ID_HEARTBEAT:
        heartbeat_system_status = mavlink_msg_heartbeat_get_system_status(msg);
        heartbeat_base_mode     = mavlink_msg_heartbeat_get_base_mode(msg);
        heartbeat_custom_mode   = mavlink_msg_heartbeat_get_custom_mode(msg);
        // we just got a heartbeat from PX4 over uart so flag as connected
        if(!is_connected){
            is_connected = 1;
            printf("PX4 Connected over UART with sysid %d\n", sysid);
        }
        break;
    case MAVLINK_MSG_ID_SYS_STATUS:
        sys_status_battery_volts = ((double) mavlink_msg_sys_status_get_voltage_battery(msg)) / 1000.0;
        sys_status_battery_percent = ((double) mavlink_msg_sys_status_get_battery_remaining(msg)) / 100.0;
        break;
    case MAVLINK_MSG_ID_ATTITUDE:
        pthread_mutex_lock(&data_mutex);
        mavlink_msg_attitude_decode(msg, &attitude);
        pthread_mutex_unlock(&data_mutex);
        break;
    default:
        break;
    }
    return;
}


int px4_monitor_is_connected(void)
{
    return is_connected;
}

uint8_t px4_monitor_get_sysid(void)
{
    return sysid;
}

int px4_monitor_is_armed(void)
{
    if(heartbeat_base_mode & MAV_MODE_FLAG_SAFETY_ARMED){
        return 1;
    }
    return 0;
}


px4_main_mode px4_monitor_get_main_mode(void)
{
    return (heartbeat_custom_mode&0x00FF0000)>>16;
}


px4_sub_mode px4_monitor_get_sub_mode(void)
{
    return (heartbeat_custom_mode&0xFF000000)>>24;
}


double px4_monitor_get_bat_voltage(void)
{
    return sys_status_battery_volts;
}


double px4_monitor_get_bat_percentage(void)
{
    return sys_status_battery_percent;
}

void px4_monitor_print_main_mode(void)
{
    px4_main_mode mode = px4_monitor_get_main_mode();
    switch(mode){
        case PX4_MAIN_MODE_UNKNOWN :
            printf("UNKNOWN");
            break;
        case PX4_MAIN_MODE_MANUAL :
            printf("MANUAL");
            break;
        case PX4_MAIN_MODE_ALTCTL :
            printf("ALTITUDE");
            break;
        case PX4_MAIN_MODE_POSCTL :
            printf("POSITION");
            break;
        case PX4_MAIN_MODE_AUTO :
            printf("AUTO");
            break;
        case PX4_MAIN_MODE_ACRO :
            printf("ACRO");
            break;
        case PX4_MAIN_MODE_OFFBOARD :
            printf("OFFBOARD");
            break;
        case PX4_MAIN_MODE_STABILIZED :
            printf("STABILIZED");
            break;
        case PX4_MAIN_MODE_RATTITUDE:
            printf("RATTITUDE");
            break;
        default:
            printf("unknown main flight mode");
    }
    return;
}


// used by the offboard mode controllers to see if PX4 is currenly obeying
// offboard commands. returns 1 if armed and in offboard mode, otherwise 0
int px4_monitor_is_armed_and_in_offboard_mode(void)
{
    // yes I could make this one big if statement but we may add extra
    // conditions in the future and this is easier to read if it gets longer.
    if(!is_connected) return 0;
    if(!px4_monitor_is_armed()) return 0;
    if(px4_monitor_get_main_mode()!=PX4_MAIN_MODE_OFFBOARD) return 0;
    return 1;
}


mavlink_attitude_t  px4_monitor_get_attitude(void)
{
    mavlink_attitude_t ret;
    pthread_mutex_lock(&data_mutex);
    ret = attitude;
    pthread_mutex_unlock(&data_mutex);
    return ret;
}


int px4_monitor_get_rpy(float* roll, float* pitch, float* yaw)
{
    if(!is_connected){
        return -1;
    }

    pthread_mutex_lock(&data_mutex);
    *roll = attitude.roll;
    *pitch = attitude.pitch;
    *yaw = attitude.yaw;
    pthread_mutex_unlock(&data_mutex);
    return 0;
}

void px4_monitor_set_connected_flag(int flag)
{
    is_connected = flag;
    return;
}