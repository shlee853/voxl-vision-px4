/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <unistd.h> // for write()
#include <modal_pipe_sink.h>
#include <voxl_vision_px4.h>
#include "px4_shell.h"
#include "uart_mavlink.h"
#include "px4_monitor.h"
#include "pipe_channels.h"


#define READ_BUF_SIZE		2048


static int from_px4_pipe_fd = 0;


static void data_cb(__attribute__((unused)) int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// make sure a null character is after the command
	data[bytes]=0;

	// construct a mavlink message
	mavlink_message_t msg;
	uint8_t device = SERIAL_CONTROL_DEV_SHELL;
	uint8_t flags = SERIAL_CONTROL_FLAG_MULTI | \
					SERIAL_CONTROL_FLAG_EXCLUSIVE | \
					SERIAL_CONTROL_FLAG_RESPOND;
	uint16_t timeout = 0; // unused
	uint32_t baudrate = 0; // unused

	// put a NULL character at the end if it's missing
	if(data[bytes-1]!=0){
		data[bytes]=0;
		bytes++;
	}

	// loop through and send in 70 byte chunks
	int bytes_left = bytes;
	while(bytes_left>0){
		// send only 70 bytes at a time
		int bytes_to_send = bytes_left;
		if(bytes_to_send>70) bytes_to_send = 70;

		mavlink_msg_serial_control_pack(px4_monitor_get_sysid(), VOXL_COMPID, &msg,\
						device, flags, timeout, baudrate, bytes_to_send, (uint8_t*)(data+bytes-bytes_left));
		// send message
		uart_mavlink_send_msg(msg);
		bytes_left-=bytes_to_send;
		//printf("sending %d bytes to px4 shell over uart: %s\n", bytes, data);
	}
	return;
}

int px4_shell_init(void)
{
	// initialize the to-px4 channel for sending commands to px4
	if(pipe_sink_init_channel(TO_PX4_SHELL_CH, TO_PX4_SHELL_PATH, 1, READ_BUF_SIZE)){
		return -1;
	}
	pipe_sink_set_simple_cb(TO_PX4_SHELL_CH, data_cb, NULL);
	// initialize the from PX4 channel for displaying responses from px4
	if(pipe_sink_init_channel(FROM_PX4_SHELL_CH, FROM_PX4_SHELL_PATH, 0, 0)){
		return -1;
	}
	from_px4_pipe_fd = pipe_sink_get_fd(FROM_PX4_SHELL_CH);
	return 0;
}

int px4_shell_stop(void)
{
	pipe_sink_close_channel(TO_PX4_SHELL_CH);
	pipe_sink_close_channel(FROM_PX4_SHELL_CH);
	return 0;
}

int px4_shell_handle_serial_msg_from_px4(mavlink_message_t msg)
{
	if(from_px4_pipe_fd==0) return -1;
	uint8_t device = mavlink_msg_serial_control_get_device(&msg);
	if(device!=SERIAL_CONTROL_DEV_SHELL) return 0;
	uint8_t count = mavlink_msg_serial_control_get_count(&msg);
	if(count<1) return -1;
	uint8_t data[72];
	// this memcopies the data out
	mavlink_msg_serial_control_get_data(&msg, data);
	write(from_px4_pipe_fd, data, count);

	// debug print
	//data[count]=0; // make sure last character is 0 to terminate the string
	//printf("recieved %d bytes from px4 over uart: %s\n", count, (char*)data);
	return 0;
}