/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <getopt.h>
#include <stdlib.h> // for exit()

#include <voxl_io.h>
#include <modal_start_stop.h>
#include "config_file.h"
#include "geometry.h"
#include "uart_mavlink.h"
#include "udp_mavlink.h"
#include "vio_manager.h"
#include "apriltag_manager.h"
#include "voa_manager.h"
#include "adsb_manager.h"
#include "px4_monitor.h"
#include "offboard_mode.h"
#include "px4_shell.h"
#include "fixed_pose_input.h"

#define PROCESS_NAME "voxl-vision-px4" // to name PID file

static int override_en_apriltag=0;
static char* override_qcg_ip=NULL;
static int override_en_voa=0;
static int override_en_vio=0;
static int override_en_adsb=0;

// printed if some invalid argument was given
static void __print_usage(void)
{
	printf("\n\
voxl-vision-px4 usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, voxl-vision-px4 will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
-a, --debug_adsb            print ADSB debug info\n\
-b, --debug_uart_send       show debug info on uart mavlink packets sending to PX4\n\
-c, --load_config_only      Load the config file and then exit right away.\n\
                              This also adds new defaults if necessary. This is used\n\
                              by voxl-configure-voxl-px4 to make sure the config file\n\
                              is up to date without actually starting this service.\n\
-d, --debug_uart_recv       show debug info on uart mavlink packets coming from PX4\n\
-e, --debug_udp_send        show debug info on UDP mavlink packets being sent\n\
-f, --debug_udp_recv        show debug info on UDP mavlink packets coming in\n\
-g, --debug_fixed_frame     print debug info regarding the calculation of fixed frame\n\
                              relative to local frame as set by apriltags.\n\
-h, --help                  print this help message\n\
-i, --qgc_ip {ip}           override the primary qGroundControl IP address\n\
                              that is specified in the config file\n\
-l, --debug_tag_local       print location and rotation of each tag in local frame\n\
                              note that apriltag frame of reference is not the same\n\
                              as local frame so interpreting roll/pitch/yaw requires\n\
                              some thought\n\
-m, --debug_tag_cam         print location and rotation of each tag detection wrt cam\n\
-o, --debug_odometry        print the odometry of body in local frame each time\n\
                              a VIO packet is sent to PX4\n\
-p, --debug_odometry_fixed  print the odometry of body in fixed frame. Note this\n\
                              is not the data being sent to px4. This is to help\n\
                              ensure fixed apriltags are set correctly in config file\n\
-s, --debug_stereo          print detected stereo obstacles as linescan points\n\
-u, --debug_offboard        print debug info for whichever offboard mode is active\n\
\n");
	return;
}


static int __parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug_adsb",            no_argument,       0, 'a'},
		{"debug_uart_send",       no_argument,       0, 'b'},
		{"load_config_only",      no_argument,       0, 'c'},
		{"debug_uart_recv",       no_argument,       0, 'd'},
		{"debug_udp_send",        no_argument,       0, 'e'},
		{"debug_udp_recv",        no_argument,       0, 'f'},
		{"debug_fixed_frame",     no_argument,       0, 'g'},
		{"help",                  no_argument,       0, 'h'},
		{"qgc_ip",                required_argument, 0, 'i'},
		{"debug_tag_local",       no_argument,       0, 'l'},
		{"debug_tag_cam",         no_argument,       0, 'm'},
		{"debug_odometry",        no_argument,       0, 'o'},
		{"debug_odometry_fixed",  no_argument,       0, 'p'},
		{"debug_stereo",          no_argument,       0, 's'},
		{"debug_offboard",        no_argument,       0, 'u'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "abcdefghi:lmopsu",
					   long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'a':
			override_en_adsb=1;
			adsb_manager_en_print_debug();
			break;

		case 'b':
			uart_mavlink_en_print_debug_send(1);
			break;

		case 'c':
			config_file_load();
			exit(0);
			break;

		case 'd':
			uart_mavlink_en_print_debug_recv(1);
			break;

		case 'e':
			udp_mavlink_en_print_debug_send(1);
			break;

		case 'f':
			udp_mavlink_en_print_debug_recv(1);
			break;

		case 'g':
			geometry_en_print_fixed_frame_debug(1);
			break;

		case 'h':
			__print_usage();
			return -1;

		case 'i':
			override_qcg_ip = optarg;
			break;

		case 'l':
			apriltag_manager_en_print_debug_wrt_local(1);
			override_en_apriltag=1;
			break;

		case 'm':
			apriltag_manager_en_print_debug_wrt_cam(1);
			override_en_apriltag=1;
			break;

		case 'o':
			vio_manager_en_print_debug_local(1);
			override_en_vio=1;
			break;

		case 'p':
			vio_manager_en_print_debug_fixed(1);
			override_en_vio=1;
			break;

		case 's':
			override_en_voa=1;
			voa_manager_en_print_linescan(1);
			break;

		case 'u':
			offboard_mode_en_print_debug(1);
			break;

		default:
			__print_usage();
			return -1;
		}
	}

	return 0;
}


// initializes everything then waits on blocking function "modalai_vl_run"
int main(int argc, char* argv[])
{
	if(__parse_opts(argc, argv)) return -1;

	printf("loading our own config file\n");
	if(config_file_load()) return -1;
	config_file_print();

	printf("loading extrinsics and apriltag external config files\n");
	if(load_common_config_files()) return -1;

	// after loading config file, apply overrides that came from arguments
	if(override_en_vio) en_vio=1;
	if(override_en_voa) en_voa=1;
	if(override_qcg_ip!=NULL) strcpy(qgc_ip, override_qcg_ip);
	if(override_en_adsb) en_adsb=1;

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal manager so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal manager\n");
		return -1;
	}

	// start the critical modules other things depend on
	printf("starting geometry module\n");
	if(geometry_init()) return -1;
	printf("starting uart mavlink\n");
	if(uart_mavlink_init()) return -1;
	printf("starting udp mavlink\n");
	if(udp_mavlink_init()) return -1;

	// start vision and other things
	printf("starting px4 shell\n");
	px4_shell_init();
	printf("starting fixed pose input\n");
	fixed_pose_input_start();
	if(en_vio){
		printf("starting vio manager\n");
		if(vio_manager_start()) return -1;
	}
	printf("starting apriltag manager\n");
	if(apriltag_manager_start()) return -1;
	if(en_voa){
		printf("starting voa manager\n");
		if(voa_manager_start()) return -1;
	}
	if(en_adsb){
		printf("starting adsb manager\n");
		adsb_manager_start();
	}
	offboard_mode_init();


	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);


////////////////////////////////////////////////////////////////////////////////
// all threads started, wait for signal manager to stop it
////////////////////////////////////////////////////////////////////////////////
	main_running=1;
	printf("Init complete, entering main loop\n");
	while(main_running) usleep(500000);
	printf("Starting shutdown sequence\n");


////////////////////////////////////////////////////////////////////////////////
// Stop all the threads
////////////////////////////////////////////////////////////////////////////////

	// then stop non-critical optional threads that may still be dependent on
	// more critical features like UDP and UART ports
	offboard_mode_stop(1); // stop in blocking mode, waits until thread joins
	printf("Stopping adsb\n");
	adsb_manager_stop();


	// stop all the vision stuff before closing comms ports since these all
	// write out to px4/qgc
	printf("stopping voa manager\n");
	voa_manager_stop();
	printf("stopping apriltag manager\n");
	apriltag_manager_stop();
	printf("stopping vio manager\n");
	vio_manager_stop();
	printf("stopping fixed pose in module\n");
	fixed_pose_input_stop();
	printf("stopping px4 shell module\n");
	px4_shell_stop();

	// now stop the critical UDP and UART mavlink interfaces
	printf("Stopping udp mavlink module\n");
	udp_mavlink_stop();
	printf("Stopping uart mavlink module\n");
	uart_mavlink_stop();

	// clean up our PID file is all was successful
	printf("Removing PID file\n");
	remove_pid_file(PROCESS_NAME);
	printf("exiting cleanly\n");
	return 0;
}
