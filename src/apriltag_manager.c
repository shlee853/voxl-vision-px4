/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include <rc_math.h>
#include <modal_pipe_client.h>

#include "macros.h"
#include "apriltag_manager.h"
#include "pipe_channels.h"
#include "geometry.h"
#include "config_file.h"
#include "offboard_follow_tag.h"


// each tag detection is 48 bytes, 1k buf gives us up to 16 tag detections each
// read from the pipe which is plenty considering we only expect 1 in normal
// operation. This is not the size of the pipe FIFO which can hold much more!
#define PIPE_READ_BUF_SIZE	1024

#define APRILTAG_PIPE (MODAL_PIPE_DEFAULT_BASE_DIR "apriltag/")

// server state exists to allow an external data server process to start and stop in the
// background without voxl-vision-px4 needing to restart.
#define SS_WAITING			0 // waiting for a connection
#define SS_DISCONNECTED		1 // just disconnected and needs closing
#define SS_CONNECTED		2 // connected and working
static int server_state;

static int running = 0; // local running flag separate from main_running
static pthread_t thread_id;
static int print_debug = 0;
static int print_debug_wrt_cam = 0;
static int print_debug_wrt_local = 0;


void apriltag_manager_en_print_debug_wrt_cam(int en_print)
{
	if(en_print){
		print_debug_wrt_cam = 1;
		print_debug = 1;
	}
	return;
}

void apriltag_manager_en_print_debug_wrt_local(int en_print)
{
	if(en_print){
		print_debug_wrt_local = 1;
		print_debug = 1;
	}
	return;
}


// This is the function called by the libmodal_pipe "simple helper thread"
// all tags are reported here and then we decide what to do with them
static void apriltag_data_cb(__attribute__((unused))int ch, char* data, int bytes, \
											__attribute__((unused)) void* context)
{
	int n, i, j;
	static rc_matrix_t R_tag_to_cam = RC_MATRIX_INITIALIZER;
	static rc_vector_t T_tag_wrt_cam = RC_VECTOR_INITIALIZER;

	// validate that the data makes sense
	apriltag_data_t* data_array = modal_apriltag_validate_pipe_data(data, bytes, &n);
	if(data_array == NULL) return;

	// process each tag
	for(i=0;i<n;i++){

		// copy out data
		float_to_matrix(data_array[i].R_tag_to_cam,  &R_tag_to_cam);
		float_to_vector(data_array[i].T_tag_wrt_cam, &T_tag_wrt_cam);
		int64_t timestamp_ns = data_array[i].timestamp_ns;
		int id = data_array[i].tag_id;

		// if apriltag fixed frame is enabled, make sure the tag id is "known"
		// and grab the index while we are at it. This will just check against
		// apriltags which were listed as having a fixed location in the
		// common apriltag config file.
		int fixed_tag_index = -1;
		if(en_apriltag_fixed_frame){
			for(j=0;j<n_fixed_apriltags;j++){
				if(fixed_apriltags[j].tag_id == id){
					fixed_tag_index = j;
					break;
				}
			}
			// if we found a record of the fixed tag, use it
			if(fixed_tag_index>=0){
				geometry_add_fixed_tag_detection(fixed_tag_index, timestamp_ns, R_tag_to_cam, T_tag_wrt_cam);
			}
		}

		// pass to offboard mode if enabled
		if(offboard_mode==FOLLOW_TAG && id==follow_tag_id){
			offboard_follow_tag_add_detection(timestamp_ns, R_tag_to_cam, T_tag_wrt_cam);
		}

		// TODO uncomment this when we get to landing
		//if(offboard_mode==LAND_ON_TAG && id==land_on_tag_id){
		//	offboard_land_on_tag_add_detection(frame_timestamp_ns, R_tag_to_cam, T_tag_wrt_cam);
		//}

		// debug prints if requested
		if(print_debug_wrt_cam){
			double roll, pitch, yaw;
			rc_rotation_to_tait_bryan(R_tag_to_cam, &roll, &pitch, &yaw);
			printf("ID %d T_tag_wrt_cam %5.2f %5.2f %5.2f  Roll %5.2f Pitch %5.2f Yaw %5.2f\n",\
						id,T_tag_wrt_cam.d[0],T_tag_wrt_cam.d[1],T_tag_wrt_cam.d[2],roll ,pitch ,yaw);
		}
		if(print_debug_wrt_local){
			static rc_matrix_t R_tag_to_local;
			static rc_vector_t T_tag_wrt_local;
			double roll, pitch, yaw;
			// ask geometry module for the position and rotation of the tag in local
			// frame. This goes back in time and interpolates given timestamp
			int ret = geometry_calc_R_T_tag_in_local_frame(timestamp_ns, R_tag_to_cam, \
							T_tag_wrt_cam, &R_tag_to_local, &T_tag_wrt_local);
			if(ret==0){
				rc_rotation_to_tait_bryan(R_tag_to_local, &roll, &pitch, &yaw);

				printf("ID%3d T_tag_wrt_local %5.2f %5.2f %5.2f  Roll %5.2f Pitch %5.2f Yaw %5.2f\n",\
						id,T_tag_wrt_local.d[0],T_tag_wrt_local.d[1],T_tag_wrt_local.d[2],roll ,pitch ,yaw);
			}
		}
	} // end of looping through each tag
	return;
}


// this thread just manages the connection to the apriltag server, waiting for
// it to become available, connecting if so, and reconnecting if the server
// restarts. It also closes the pipe connection on exit. No actual processing.
static void* apriltag_pipe_monitor(__attribute__((unused)) void* arg)
{
	while(running){
		if(server_state==SS_WAITING){
			// attempt to connect to the server expecting it might fail
			int ret = pipe_client_init_channel(APRILTAG_PIPE_CH, APRILTAG_PIPE, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_SIMPLE_HELPER, PIPE_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to the server
				server_state = SS_CONNECTED;
				pipe_client_set_simple_helper_cb(APRILTAG_PIPE_CH, apriltag_data_cb, NULL);
				continue;
			}

		}
		else if(server_state==SS_DISCONNECTED){
			// cleanup if disconnected and go back to waiting
			pipe_client_close_channel(APRILTAG_PIPE_CH);
			server_state = SS_WAITING;
		}
		// otherwise all is fine.
		usleep(500000);
	}

	// close channel when we exit
	pipe_client_close_channel(APRILTAG_PIPE_CH);
	return NULL;
}


// start the pipe monitor thread which will open the apriltag pipe and helper
int apriltag_manager_start(void)
{
	if(running){
		fprintf(stderr, "ERROR in %s, already running\n", __FUNCTION__);
		return 0;
	}
	// start the manager thread, this will open the pipe to read apriltag data
	running = 1;
	pthread_create(&thread_id, NULL, apriltag_pipe_monitor, NULL);
	return 0;
}


void apriltag_manager_stop(void)
{
	if(running==0){
		fprintf(stderr, "ERROR in %s, not running\n", __FUNCTION__);
		return;
	}
	// stop the thread, this will close the pipe
	running = 0;
	pthread_join(thread_id, NULL);
	return;
}
