#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CONFIG_FILE=/etc/modalai/voxl-vision-px4.conf
USER=$(whoami)


print_usage () {
	echo "General Usage:"
	echo "voxl-configure-vision-px4"
	echo ""
	echo "To perform factory configuration for VOXL Flight Deck"
	echo "voxl-configure-vision-px4 -f"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-vision-px4 -h"
	echo ""
	exit 0
}


## set most parameters which don't have quotes in json
set_param () {
	if [ "$#" != "2" ]; then
		echo "set_param expected 2 args"
		exit 1
	fi

	# remove quotes if they exist
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	val=$2
	val="${val%\"}"
	val="${val#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	$val," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_param_string () {
	if [ "$#" != "2" ]; then
		echo "set_param_string expected 2 args"
		exit 1
	fi

	var=$1
	var="${var%\"}"
	var="${var#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	\"$2\"," ${CONFIG_FILE}
}

enable_service_and_exit () {
	systemctl daemon-reload
	echo "enabling voxl-vision-px4 systemd service"
	systemctl enable voxl-vision-px4.service
	echo "starting voxl-vision-px4 systemd service"
	systemctl restart voxl-vision-px4.service
	echo "DONE configuring voxl-vision-px4"
	exit 0
}

## print help message if requested
if [ "$1" == "-h" ]; then
	print_usage
	exit 0
elif [ "$1" == "--help" ]; then
	print_usage
	exit 0
fi

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Run this script as the root user"
	exit 1
fi

## perform factory setup if requested
if [ "$1" == "-f" ]; then
	# delete our own config file and reload defaults
	rm /etc/modalai/voxl-vision-px4.conf
	voxl-vision-px4 -c
	enable_service_and_exit
fi


# have the voxl-vision-px4 binary open and close the config file
# this will clean it up and add any new values from updates
echo "loading and updating config file with voxl-vision-px4 -c"
voxl-vision-px4 -c


# ask if the user wants the service enabled or not
echo " "
echo "Do you want to enable the voxl-vision-px4 service to allow"
echo "communication with a PX4 flight controller?"
select opt in "yes" "no"; do
case $opt in
yes )
	echo " "
	echo "Now we are going to do a preliminary configuration of ${CONFIG_FILE}"
	break;;
no )
	echo "Disabling voxl-vision-px4 service"
	systemctl disable voxl-vision-px4
	systemctl stop voxl-vision-px4
	echo "Done disabling voxl-vision-px4"
	exit 0
	break;;
*)
	echo "invalid option"
	esac
done




## ask qgc_ip question
echo " "
echo "Which IP address is your QGroundControl station at?"
echo "If you do not wish to hard-code the QGC IP address into VOXL then you will"
echo "need to configure VOXL's IP address in QGC's Comm Links settings page."
echo "Just press enter to leave this option as-is. "
read QGCIP
if [ "${QGCIP}" != "" ]; then
	echo "setting qgc_ip to ${QGCIP}"
	set_param_string qgc_ip ${QGCIP}
fi



## ask figure eight question
echo " "
echo "Do you want to enable voxl-vision-px4 to command PX4"
echo "to fly a figure eight path when flipped into offboard mode?"
echo "Don't enable this if you intend to do your own offboard"
echo "control via MAVROS or similar."
select opt in "yes" "no"; do
case $opt in
yes )
	set_param_string offboard_mode "figure_eight"
	break;;
no )
	set_param_string offboard_mode "off"
	break;;
*)
	echo "invalid option"
	esac
done




# all done!
enable_service_and_exit

