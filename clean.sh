#!/bin/bash
#
# Modal AI Inc. 2019
# author: james@modalai.com



sudo rm -rf build/
sudo rm -rf ipk/control.tar.gz
sudo rm -rf ipk/data/
sudo rm -rf ipk/data.tar.gz
sudo rm -rf *.ipk
sudo rm -rf .bash_history