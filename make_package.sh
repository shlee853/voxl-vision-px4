#!/bin/bash
################################################################################
# Copyright (c) 2021 ModalAI, Inc. All rights reserved.
#
# be sure to build everything first with build.sh in docker
# run this on host pc
# UPDATE VERSION IN CONTROL FILE, NOT HERE!!!
#
# author: james@modalai.com
################################################################################

set -e # exit on error to prevent bad ipk from being generated

################################################################################
# variables
################################################################################
VERSION=$(cat ipk/control/control | grep "Version" | cut -d' ' -f 2)
PACKAGE=$(cat ipk/control/control | grep "Package" | cut -d' ' -f 2)
IPK_NAME=${PACKAGE}_${VERSION}.ipk

DATA_DIR=ipk/data
CONTROL_DIR=ipk/control

echo ""
echo "Package Name: " $PACKAGE
echo "version Number: " $VERSION

################################################################################
# start with a little cleanup to remove old files
################################################################################
sudo rm -rf ipk/control.tar.gz
sudo rm -rf $DATA_DIR/
sudo rm -rf ipk/data.tar.gz
sudo rm -rf $IPK_NAME

################################################################################
## Install everything into data directory.
## Do everything here with sudo since the ownership of every file and directory
## in the ipk data directory needs to match what it should be on VOXL.
################################################################################

cd build && sudo make DESTDIR=../ipk/data PREFIX=/usr install && cd -

sudo mkdir -p $DATA_DIR/etc/systemd/system/
sudo cp service/* $DATA_DIR/etc/systemd/system/
sudo cp config/voxl-configure-vision-px4.sh $DATA_DIR/usr/bin/voxl-configure-vision-px4
sudo chmod +x $DATA_DIR/usr/bin/voxl-configure-vision-px4

################################################################################
# pack the control, data, and final ipk archives
################################################################################

cd $CONTROL_DIR/
tar --create --gzip -f ../control.tar.gz *
cd ../../

cd $DATA_DIR/
tar --create --gzip -f ../data.tar.gz *
cd ../../

ar -r $IPK_NAME ipk/control.tar.gz ipk/data.tar.gz ipk/debian-binary

echo ""
echo "Done making $IPK_NAME"