/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <voxl_common_config.h>
#include <rc_math.h>


////////////////////////////////////////////////////////////////////////////////
// stuff from common config
////////////////////////////////////////////////////////////////////////////////

/*
 * The common apriltag config in libmodal_json stores translation and rotation
 * as float arrays. We convert to rc_vector and rc_matrix here for the math lib.
 * R_tag_to_fixed and T_tag_wrt_fixed are read straight from apritlags.conf The
 * other 2 are caluclated when the config file loads for later use
 */
typedef struct fixed_apriltag_t{
    int tag_id;
    rc_matrix_t R_tag_to_fixed;
    rc_vector_t T_tag_wrt_fixed;
    rc_matrix_t R_fixed_to_tag;
    rc_vector_t T_fixed_wrt_tag;
} fixed_apriltag_t;


/**
 * These are configurations parameters that come from the common config files
 * for apriltags and extrinsics. We parse those files and pick out just the
 * data that we need here. We convert the vectors and matrices from array to
 * rc_vector_t and rc_matrix_t types during parsing for use with the geometry
 * module and other parts of this file.
 */

// from apriltags.conf
extern int n_fixed_apriltags;
extern fixed_apriltag_t fixed_apriltags[VCC_MAX_APRILTAGS_IN_CONFIG];
// from extrinsics.conf
extern rc_matrix_t R_imu_to_body;
extern rc_vector_t T_imu_wrt_body;
extern double height_body_above_ground_m;
extern rc_vector_t T_stereo_wrt_body;
extern rc_matrix_t R_stereo_to_body;
extern rc_vector_t T_vio_ga_wrt_local;
extern rc_matrix_t R_vio_ga_wrt_local;

/*
 * load the common apriltag and extrinsics config files
 * this prints out data as it goes
 */
int load_common_config_files(void);


////////////////////////////////////////////////////////////////////////////////
// stuff from our own config file
////////////////////////////////////////////////////////////////////////////////

#define VOXL_VISION_PX4_CONF_FILE "/etc/modalai/voxl-vision-px4.conf"

#define OFFBOARD_STRINGS {"off","figure_eight","follow_tag"}
typedef enum offboard_mode_t{
    OFF,
    FIGURE_EIGHT,
    FOLLOW_TAG
}offboard_mode_t;


// these are all the possible parameters from the json config file
// UDP mavlink router
extern char qgc_ip[64];
extern int en_localhost_mavlink_udp;
extern int en_secondary_qgc;
extern char secondary_qgc_ip[64];
extern int qgc_udp_port_number;
extern int localhost_udp_port_number;
// basic visual features
extern int en_vio;
extern int en_voa;
extern int en_send_vio_to_qgc;
extern int en_send_voa_to_qgc;
// adsb
extern int en_adsb;
extern int adsb_uart_bus;
extern int adsb_uart_baudrate;
// px4 uart
extern int px4_uart_bus;
extern int px4_uart_baudrate;
// offboard mode
extern offboard_mode_t offboard_mode;
extern int follow_tag_id;
// fixed frame
extern int en_apriltag_fixed_frame;
extern int fixed_frame_filter_len;
extern int en_transform_mavlink_pos_setpoints_from_fixed_frame;


// load only our own config file without printing the contents
int config_file_load(void);

// prints the current configuration values to the screen.
int config_file_print(void);



#endif // end #define CONFIG_FILE_H