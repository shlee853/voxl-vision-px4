/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>	// for fprintf
#include <unistd.h>	// for read() & write()
#include <errno.h>	// to check error in read() and write()
#include <fcntl.h>	// for O_WRONLY & O_RDONLY
#include <string.h>	// for strlen()
#include <stdlib.h>	// for exit()
#include <pthread.h>

#include <voxl_vision_px4.h>

#define BUFLEN 4096

static int running;
static int to_px4_fd;
static int from_px4_fd;
static char last_cmd[BUFLEN];
static int last_cmd_len = 0;

// helper threads
static void*		__read_func(void* context);
static pthread_t	read_thread;


static void __send_cmd_to_px4(char* buf, int bytes)
{
	// correctly terminate command and send
	buf[bytes+1] = 0;
	buf[bytes] = '\n';
	sprintf(last_cmd,"%s", buf);
	last_cmd_len = bytes;
	write(to_px4_fd, buf, bytes+2);
	// printf("last_cmd_len: %d\n", last_cmd_len);
	// printf("sending: %s",buf);
	return;
}

// reader thread
static void* __read_func(__attribute__((unused))void* context)
{
	char buf[BUFLEN];
	int bytes_read;
	int promptlen = 9;
	char prompt[] = {10,110,115,104,62,32,27,91,75,0};
	int packet_done = 0;

	// keep reading from
	while(running){

		// loop until we have read a whole command. px4 sends data in 69 byte chunks
		packet_done = 0;
		bytes_read = 0;
		while(!packet_done){
			int ret = read(from_px4_fd, buf+bytes_read, BUFLEN-bytes_read-1);
			if(ret<=0){
				fprintf(stderr, "warning, read returned %d\n", ret);
				fprintf(stderr, "voxl-vision-px4 likely disconnected\n");
				exit(-1);
			}
			bytes_read+=ret;
			if(ret<69) packet_done=1;
			// exit is we are close to filling buffer
			if(bytes_read>=BUFLEN-1){
				fprintf(stderr, "WARNING read buffer full\n");
				packet_done = 1;
			}
			if(bytes_read==69) usleep(10000);
		}

		// make sure we terminate the string
		buf[bytes_read]=0;

		// debug prints
		//printf("read %d bytes\n", bytes_read);
		// for(int i=0;i<bytes_read;i++) printf("%d ",buf[i]);
		// printf("\n");

		// check if px4 echoed our last command back to us which is always
		int start_pos = 0;
		int compare = strncmp(buf,last_cmd,last_cmd_len);
		if(last_cmd_len>0 && compare==0){
			// printf("found command!\n");
			start_pos = last_cmd_len;
		}

		// check if message ends with a prompt
		if(bytes_read>=promptlen &&\
			strncmp(buf+bytes_read-promptlen, prompt, promptlen)==0){
			//printf("found prompt!\n");
			buf[bytes_read-promptlen]=0;
		}

		// print the received message and a fresh prompt
		printf("%s",buf+start_pos);
		fflush(stdout);
	}

	return NULL;
}


int main(int argc, char *argv[])
{
	char buf[BUFLEN];

	// open pipes
	from_px4_fd = open(FROM_PX4_SHELL_PATH, O_RDONLY);
	if(from_px4_fd<0){
		perror("ERROR opening from-px4 pipe");
		fprintf(stderr,"most likely voxl-vision-px4 service is not running\n");
		fprintf(stderr, "systemctl start voxl-vision-px4\n");
		return -1;
	}
	to_px4_fd = open(TO_PX4_SHELL_PATH, O_WRONLY);
	if(to_px4_fd<0){
		perror("ERROR opening to-px4 pipe");
		fprintf(stderr,"most likely voxl-vision-px4 service is not running\n");
		fprintf(stderr, "systemctl start voxl-vision-px4\n");
		return -1;
	}

	// start by showing the prompt
	if(argc<=1){
		printf("Welcome to PX4's Nuttx Shell\n");
		printf("hit Ctrl-C to exit and return to VOXL\n");
		printf("nsh> ");
		fflush(stdout);
	}

	// start read thread
	running = 1;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&read_thread, &attr, __read_func, NULL);
	pthread_attr_destroy(&attr);

	// if user gave a command to send as an argument, send it and return
	if(argc > 1){
		// construct one string from all arguments
		buf[0]=0;
		for(int i=1; i<=(argc-1); i++){
			strcat(buf, argv[i]);
			strcat(buf, " ");
		}
		// sne to px4 in the same way as normal
		__send_cmd_to_px4(buf, strlen(buf)-1);
		// wait for response to print and exit
		usleep(200000);
		printf("\n");
		return 0;
	}

	// send enter to wake up shell, not critical but helps flush out pipe
	usleep(10000);
	write(to_px4_fd, "\n", 1);

	// read user input from stdin and push to PX4
	while(running){
		int bytes = read(STDIN_FILENO, buf, BUFLEN);

		// check for accidentally trying to exit
		if(strncmp(buf,"exit\n",5)==0){
			printf("\n");
			printf("you tried to send the exit command which will detatch PX4 from\n");
			printf("this shell and you won't be able to reconnect without rebooting px4\n");
			printf("to exit out to VOXL instead press Ctrl-C\n");
			printf("nsh> ");
			fflush(stdout);
			continue;
		}
		__send_cmd_to_px4(buf,bytes);

		// if sending reboot command, wait 3 seconds then wake up the shell again
		if(strncmp(buf,"reboot\n",7)==0){
			printf("rebooting px4.");
			usleep(1000000);
			printf(".");
			fflush(stdout);
			usleep(1000000);
			printf(".");
			fflush(stdout);
			usleep(1000000);
			printf(".");
			fflush(stdout);
			write(to_px4_fd, "\n", 1);
		}
	}

	return 0;
}

